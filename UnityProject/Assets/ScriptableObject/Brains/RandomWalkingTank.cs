﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName="Brains/Random walker")]
public class RandomWalkingTank : TankBrain
{
	[SerializeField]
	[MinMaxRange(0, 10)]
	private RangedFloat idleTime = default;
	[SerializeField]
	[MinMaxRange(0, 10)]
	private RangedFloat moveTime = default;
	[SerializeField]
	[MinMaxRange(0, 10)]
	private RangedFloat fireTime = default;

	public enum States
	{
		Idle,
		Moving,
		Firing
	}

	public override void Initialize(TankThinker tank)
	{
		tank.Remember("state", States.Idle);
		tank.Remember("stateTimeout", Random.Range(idleTime.minValue, idleTime.maxValue));
	}

	public override void Think(TankThinker tank)
	{
		float stateTimeout = tank.Remember<float>("stateTimeout");
		stateTimeout -= Time.deltaTime;
		tank.Remember("stateTimeout", stateTimeout);

		var state = tank.Remember<States>("state");

		var move = tank.GetComponent<TankMovement>();
		if (state == States.Moving)
		{
			move.Steer(tank.Remember<float>("moveForwardBack"), tank.Remember<float>("moveLeftRight"));
		}
		else
		{
			move.Steer(0, 0);
		}

		if (stateTimeout < 0)
		{
			switch (state)
			{
				case States.Idle:
					{
						tank.Remember("state", States.Moving);
						tank.Remember("stateTimeout", Random.Range(moveTime.minValue, moveTime.maxValue));
						tank.Remember("moveForwardBack", Random.Range(-1f, 1f));
						tank.Remember("moveLeftRight", Random.Range(-1f, 1f));
						break;
					}
				case States.Moving:
					{
						tank.Remember("state", States.Firing);
						tank.Remember("stateTimeout", Random.Range(fireTime.minValue, fireTime.maxValue));

						var tankFire = tank.GetComponent<TankShooting>();
						tankFire.BeginChargingShot();

						break;
					}
				case States.Firing:
					{
						//Ensure the ammo component is attatched to the tank
						TankAmmo ammo = default;
						tank.TryGetComponent<TankAmmo>(out ammo);

						if (ammo != null)
						{
							bool canFire = false;

							//If the curent ammo is not empty, the tank can fire
							if (ammo.GetCanFire())
							{
								canFire = true;
							}
							else
							{
								//If the current amo type is empty, check if theyre all empty
								//if not move onto the next one
								if (!ammo.GetAllAmmoEmpty())
								{
									ammo.SwitchCurrentAmmoType(1);
									canFire = true;
								}
							}

							if (canFire)
							{
								var tankFire = tank.GetComponent<TankShooting>();
								tankFire.FireChargedShot();

								tank.Remember("state", States.Idle);
								tank.Remember("stateTimeout", Random.Range(idleTime.minValue, idleTime.maxValue));
							}
						}
						break;
					}
			}
		}
	}
}
