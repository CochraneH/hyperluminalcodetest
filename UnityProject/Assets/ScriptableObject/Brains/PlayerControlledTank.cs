﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName="Brains/Player Controlled")]
public class PlayerControlledTank : TankBrain
{
	[SerializeField]
	private int PlayerNumber = default;
	private string m_MovementAxisName = default;
	private string m_TurnAxisName = default;
	private string m_FireButton = default;

	//-----------------------------//
	private string _ammoSwitchButton = default;
	private TankAmmo _tankAmmo = default;

	public void OnEnable()
	{
		m_MovementAxisName = "Vertical" + PlayerNumber;
		m_TurnAxisName = "Horizontal" + PlayerNumber;
		m_FireButton = "Fire" + PlayerNumber;
		_ammoSwitchButton = "Switch" + PlayerNumber;
	}

	public override void Think(TankThinker tank)
	{
		var movement = tank.GetComponent<TankMovement>();

		movement.Steer(Input.GetAxis(m_MovementAxisName), Input.GetAxis(m_TurnAxisName));

		var shooting = tank.GetComponent<TankShooting>();

		//Try to get the cmponenet, safety check before actually attempting to use it
		tank.TryGetComponent<TankAmmo>(out _tankAmmo);


		if (Input.GetButton(m_FireButton) && _tankAmmo.GetCanFire())
			shooting.BeginChargingShot();
		else
			shooting.FireChargedShot();

		//If the player is firing, check if this isn't possible and play sound clip
		//this is to prevent the sound clip playing multiples when holding down the charge button
		if (Input.GetButtonDown(m_FireButton))
		{
			if(!_tankAmmo.GetCanFire())
			{
				_tankAmmo.PlayAmmoEmpty();
			}
		}

		//If the component is not null, check for input
		if (_tankAmmo != null)
		{
			if ((Input.GetButtonDown(_ammoSwitchButton)))
			{
				_tankAmmo.SwitchCurrentAmmoType(1);
			}
		}
	}
}
