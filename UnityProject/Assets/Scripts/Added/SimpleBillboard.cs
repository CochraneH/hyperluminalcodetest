﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleBillboard : MonoBehaviour
{
	private Camera _mainCamera = default;
	private Vector3 _lookAt;

	private void Start()
	{
		_mainCamera = Camera.main;
	}
	// Update is called once per frame
	void LateUpdate()
    {
		transform.LookAt(_mainCamera.transform);
    }
}
