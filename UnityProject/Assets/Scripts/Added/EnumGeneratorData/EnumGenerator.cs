using System.Collections;
using System.Collections.Generic;
using UnityEngine;


#if UNITY_EDITOR
using UnityEditor;
using System.IO;

public class GenerateEnum
{
    [MenuItem("Tools/GenerateAmmoTypes")]
    public static void GoMonster()
    {
        string enumName = "Ammo";
        string[] enumEntries = EnumGenerator.Instance.GetAmmoList().ToArray();
        string filePathAndName = "Assets/Scripts/Added/EnumGeneratorData/" + enumName + ".cs";

        GenerateEnums(filePathAndName, enumEntries, "Ammo");
    }

    public static void GenerateEnums(string path, string[] s, string name)
    {
        using (StreamWriter streamWriter = new StreamWriter(path))
        {
            streamWriter.WriteLine("public enum " + name);
            streamWriter.WriteLine("{");
            for (int i = 0; i < s.Length; i++)
            {
                streamWriter.WriteLine("\t" + s[i] + ",");
            }
            streamWriter.WriteLine("}");
        }
        AssetDatabase.Refresh();
    }
}
#endif

public class EnumGenerator : MonoBehaviour
{
    public static EnumGenerator Instance = default;

    [SerializeField]
    private List<string> ammoTypes = new List<string>();


    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    public List<string> GetAmmoList()
    {
        return ammoTypes;
    }


    private void OnDrawGizmos()
	{
		if(Instance == null)
		{
            Instance = this;
		}
	}
}
