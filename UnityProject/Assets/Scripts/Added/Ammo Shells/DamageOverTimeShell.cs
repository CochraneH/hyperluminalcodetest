﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOverTimeShell : ShellExplosion
{
    [SerializeField]
    [Range(1, 20)]
    private float _chipDamage = 0;

    [SerializeField]
    [Range(0.5f, 10)]
    [Tooltip("Length of time the tank will take damage for")]
    private float _damageTime = 0;

    [SerializeField]
    [Range(0.1f, 5)]
    [Tooltip("Time between taking damage over the length of damage time")]
    private float _damageInterval = 1f;

    //Only applies damage over time to the affected tank
	public override void Impact(Collider[] colliders)
	{
        foreach(Collider c in colliders)
		{
            TankHealth _tankHealth = default;
            c.TryGetComponent<TankHealth>(out _tankHealth);

            if(_tankHealth != null)
			{
                _tankHealth.ApplyDamageOverTime(_chipDamage, _damageTime, _damageInterval);
			}
		}
	}
}
