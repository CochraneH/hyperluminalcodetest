﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Ammo Type", menuName = "Create New Ammo", order = -1)]
public class AmmoType : ScriptableObject
{
    [SerializeField]
    private Ammo _ammo = default;

    [SerializeField]
    [Range(1, 10)]
    private int ammoCap = 1;

    [SerializeField]
    [Range(10, 200)]
    private int _maxDamage = 100;

    [SerializeField]
    [Range(30, 50)]
    private float _maxLaunchForce = 20;

    [SerializeField]
    [Range(10, 30)]
    private float _minLaunchForce = 20;

    [SerializeField]
    [Tooltip("The colour of this material will be used for the player ammo info UI")]
    private Material mat = default;

    [SerializeField]
    private Rigidbody _shellType = default;

    public Material GetMat()
	{
        return mat;
	}
    public int GetAmmoCap()
	{
        return ammoCap;
	}

    public int GetDamage()
	{
        return _maxDamage;
	}

    public Ammo GetAmmoName()
	{
        return _ammo;
	}

    public float GetMaxLaunchForce()
	{
        return _maxLaunchForce;
	}

    public float GetMinLaunchForce()
    {
        return _minLaunchForce;
    }

    public Rigidbody GetShellType()
	{
        return _shellType;
	}
}
