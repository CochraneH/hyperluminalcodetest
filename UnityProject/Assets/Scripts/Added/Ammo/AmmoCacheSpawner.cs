﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AmmoCacheData
{
    [SerializeField]
    private Ammo _ammoName = default;
    [SerializeField]
    private Material _ammoMat = default;

    public Ammo GetAmmoName()
	{
        return _ammoName;
	}
    public Material GetAmmoMat()
	{
        return _ammoMat;
	}
}

public class AmmoCacheSpawner : MonoBehaviour
{
    public static AmmoCacheSpawner Instance = default;

    [SerializeField]
    private List<Transform> cacheSpawns = new List<Transform>();

    [SerializeField]
    private int _maxCacheSpawns = 1;

    [SerializeField]
    private List<AmmoCacheData> cacheData = new List<AmmoCacheData>();

    private List<Transform> avaliableCacheSpawns = new List<Transform>();

    [SerializeField]
    private Material _defaultMat = default;

    [SerializeField]
    private int _timeBetweenCacheSpawns = 1;

    [SerializeField]
    private GameObject _ammoCachePrefab = default;

    //Current spawned caches in the map
    private int spawnedCaches = 0;

    [SerializeField]
    private AudioSource _source = default;

    [SerializeField]
    private AudioClip _ammoSpawned = default;

    [SerializeField]
    private AudioClip _ammoTaken = default;

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;

        //Limit the total number of spawnable caches to prevent error
        Mathf.Clamp(spawnedCaches, 0, cacheSpawns.Count -1);

        //Fill the avaliable spawns
        avaliableCacheSpawns.AddRange(cacheSpawns);
    }

    IEnumerator CacheSpawn()
	{
        while(true)
		{
            yield return new WaitForSeconds(_timeBetweenCacheSpawns);
            SpawnNewCache();
		}
	}

    //Called when the main scene opens
    public void StartCacheSpanws()
	{
        StartCoroutine(CacheSpawn());
    }

    public void SpawnNewCache()
    {
        if (spawnedCaches <= _maxCacheSpawns)
        {
            //If there are no more avaliable cache spawns, fill with the originals
            if (avaliableCacheSpawns.Count == 0)
            {
                avaliableCacheSpawns.AddRange(cacheSpawns);
            }

            //Grab a random spawn point
            int spawnPoint = Random.Range(0, avaliableCacheSpawns.Count - 1);

            //Assign the spawn point of the newest cache
            Transform spawn = avaliableCacheSpawns[spawnPoint];

            //Remove this point from the cache transaforms to prevent overlap
            avaliableCacheSpawns.RemoveAt(spawnPoint);

            //Pick a random ammo type
            Ammo ammoToSpawn = (Ammo)Random.Range(0, 3);

            //Create the cache
            GameObject cacheObject = Instantiate(_ammoCachePrefab, spawn.position, Quaternion.identity);

            AmmoCache cache = default;
            cacheObject.TryGetComponent<AmmoCache>(out cache);

            //Assign the cache amo type
            if (cache != null)
            {
                //Assign the cache the correct colours based on their ammo type
                cache.SetAmmoType(ammoToSpawn, GetAmmoMaterial(ammoToSpawn)); 
            }

            spawnedCaches++;
            _source.clip = _ammoSpawned;
            _source.Play();
        }
    }

    //Once a chache has been collected, allow for another to be spawned
    public void RemoveCache()
	{
        spawnedCaches--;
        _source.clip = _ammoTaken;
        _source.Play();
	}

    //Return the material associated with the ammo type
    //return a default if not found to prevent errors
    public Material GetAmmoMaterial(Ammo type)
    {
        foreach (AmmoCacheData d in cacheData)
        {
            if (d.GetAmmoName() == type)
            {
                return d.GetAmmoMat();
            }
        }

        return _defaultMat;
    }
}
