﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[System.Serializable]
public class TankAmmoContainer
{
    [SerializeField]
    private AmmoType _ammo = default;

    private int _ammoCapacity = default;

    private Material _mat = default;

    public void AssignAmmoCapacity()
	{
        _ammoCapacity = _ammo.GetAmmoCap();
        _mat = _ammo.GetMat();
	}

    public void SetMaterial(Material m)
	{
        _mat = m;
	}

    public Material GetMaterial()
	{
        return _mat;
	}

    public AmmoType GetAmmoType()
	{
        return _ammo;
	}

    public int GetAmmoCapacity()
	{
        return _ammoCapacity;
	}

    public void FireAmmo()
	{
        _ammoCapacity -= 1;
	}

    public void ResupplyAmmoCapacity()
	{
        _ammoCapacity = _ammo.GetAmmoCap();
	}

    public int GetAmmoCap()
	{
        return _ammo.GetAmmoCap();
	}
}

public class TankAmmo : MonoBehaviour
{
    [SerializeField]
    private List<TankAmmoContainer> avaliableAmmoTypes = new List<TankAmmoContainer>();

    private int currentAmmoType = -1;

    private TankAmmoContainer _currentAmmo = default;

    [SerializeField]
    [Tooltip("Text game object that handles the UI for ammo")]
    private TextMeshProUGUI ammoCapacity = default;

    [SerializeField]
    private AudioSource _source = default;

    [SerializeField]
    private AudioClip _emptyAmmoClip = default;

    // Start is called before the first frame update
    void Start()
    {
        //Fill up each ammo type capacity to start
        foreach(TankAmmoContainer t in avaliableAmmoTypes)
		{
            t.AssignAmmoCapacity();
        }

        SwitchCurrentAmmoType(1);
    }

    //Switch the current ammo the tank is using
    public void SwitchCurrentAmmoType(int i)
	{
        currentAmmoType += i;

        if(currentAmmoType > avaliableAmmoTypes.Count - 1)
		{
            currentAmmoType = 0;
		}
        else if(currentAmmoType <= 0)
		{
            currentAmmoType = avaliableAmmoTypes.Count - 1;
		}

        _currentAmmo = avaliableAmmoTypes[currentAmmoType];

        UpdateAmmo();
	}

    //Check if the current tank can fire/has avaliable ammo
    public bool GetCanFire()
	{
        if(_currentAmmo.GetAmmoCapacity() > 0)
		{
            return true;
		}
        return false;
	}

    public void Fire()
	{
        _currentAmmo.FireAmmo();
        UpdateAmmo();
    }

    //Get the current ammo type
    public AmmoType GetCurrentAmmoType()
    {
        if (_currentAmmo != null)
        {
            return _currentAmmo.GetAmmoType();
        }

        return avaliableAmmoTypes[0].GetAmmoType();
    }

    //Resupply the ammo of the given cache, passing in a negative
    //will resupply the tank with the original cap
    public void ResupplyAmmoCapacity(Ammo type)
	{
        foreach(TankAmmoContainer a in avaliableAmmoTypes)
		{
            if(a.GetAmmoType().GetAmmoName() == type)
			{
                _currentAmmo = a;
                a.ResupplyAmmoCapacity();
                break;
			}
		}

        UpdateAmmo();
	}

    public bool GetAllAmmoEmpty()
	{
        foreach(TankAmmoContainer a in avaliableAmmoTypes)
		{
            if(a.GetAmmoCapacity() != 0)
			{
                return false;
			}
		}
        return true;
	}

    //Allow player to know ammo is empty with a sound clip
    public void PlayAmmoEmpty()
	{
        _source.clip = _emptyAmmoClip;
        _source.Play();
	}

    //Update the UI elements relating to ammo
    public void UpdateAmmo()
    {
        ammoCapacity.text = _currentAmmo.GetAmmoCapacity() + "/" + _currentAmmo.GetAmmoCap();

        //Update the UI colour based on ammo
        ammoCapacity.color = _currentAmmo.GetMaterial().color;
    }

	private void OnTriggerEnter(Collider other)
	{
        AmmoCache cache = default;
        other.TryGetComponent<AmmoCache>(out cache);

        if(cache != null)
		{
            ResupplyAmmoCapacity(cache.GetAmmoType());
            cache.RefillAmmo();
		}
	}
}
