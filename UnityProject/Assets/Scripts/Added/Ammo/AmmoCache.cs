﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AmmoCache : MonoBehaviour
{
    [SerializeField]
    [Tooltip("The UI text to show the ammo type")]
    private TextMeshProUGUI _ammoName = default;

    private Ammo _ammoType = default;

    private Renderer _rend = default;

    //Called when the ammo cache is spawned
    public void SetAmmoType(Ammo t, Material m)
	{
        _ammoType = t;
        _ammoName.text = "Ammo Cache: " + t.ToString();
        _ammoName.color = m.color;

        _rend = GetComponent<Renderer>();
        _rend.sharedMaterial = m;
	}

    public Ammo GetAmmoType()
	{
        return _ammoType;
	}

    //Once a tank colliders with the cache trigger, the cache destroys itself and refills the tank ammo
    public void RefillAmmo()
    {
        AmmoCacheSpawner.Instance.RemoveCache();
        Destroy(gameObject);
    }
}
